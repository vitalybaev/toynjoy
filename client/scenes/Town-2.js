import BaseScene from '../utilities/base-scene';
import {DOWN, UP} from '../../shared/constants/directions';
import {HOUSE_1, HOUSE_2, TOWN, TOWN_2} from '../../shared/constants/scenes';
import {MAP_TOWN_2, IMAGE_TOWN_2} from '../constants/assets';

class Town_2 extends BaseScene {


    constructor() {
        super(TOWN_2);
    }

    init(data) {
        super.init({x: 352, y: 1216, direction: UP});
    }

    create() {
        super.create(MAP_TOWN_2, IMAGE_TOWN_2, false);
    }


    registerCollision() {
        console.log(this.layers[1])
        this.layers[1].setCollisionByProperty({ collides: true });
        this.layers[2].setDepth(10);


        let player = this.player.players[this.player.socket.id];

        this.physics.add.collider(player, this.layers[1]);
    }


    getPosition(data) {
        console.log(data);
    }

}

export default Town_2;