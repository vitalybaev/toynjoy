export const IMAGE_PLAYER = 'player';
export const IMAGE_HOUSE = 'house';
export const IMAGE_TOWN = 'town';
export const IMAGE_TOWN_2 = 'town-2';

export const MAP_HOUSE_1 = 'map-house-1';
export const MAP_HOUSE_2 = 'map-house-2';
export const MAP_TOWN = 'map-town';
export const MAP_TOWN_2 = 'map-town-2';
